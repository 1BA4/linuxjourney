# Red Hat Enterprise Linux


<b>Overview</b>
Red Hat Enterprise Linux commonly referred to as RHEL is developed by Red Hat. RHEL has strict rules to restrict free re-distribution although it still provides source code for free.

<b>Package Management</b>
RHEL uses a different package manager than Debian, RPM package manager.

<b>Configurability</b>
RHEL-based operating systems will differ slightly from the Debian-based operating systems, most noticeably in package management. If you decide to go with RHEL it’s probably best if you know you’ll be working with it.

<b>Uses</b>
RHEL is mostly used in the enterprise for its stability and support.
