# lsusb, lspci, lssci

## Lesson Content

Just like we would use the ls command to list files and directories, we can use similar tools that list information about devices.

<b>Listing USB Devices</b>
```
lsusb
```

<b>Listing PCI Devices</b>
```
lspci
```

<b>Listing SCSI Devices</b>
```
lsscsi
```
